# OpenScope.org Web

Ez a tároló tartalmazza az https://openscope.org/ weblapjait. Legnagyobb részt statikus HTML tartalomról van szó, minimális JavaScript komponensekkel.

## Összeállítás

A projekt a `hugo` statikus weboldal-generátort használja, azzal lehet előállítani a végleges fájlokat. A célmappát a `-d` kapcsolóval lehet megadni:

    hugo -d result

## Futtatás helyi számítógépen

A projekt jellegéből adódan nem szükséges „rendes” webszerverrel használni, a `hugo`-val is futtatható:

    git clone git@gitlab.com:fsfhu/openscope-web.git
    cd openscope-web
    hugo server

Ezután elérhető a http://localhost:1313/ címen.
