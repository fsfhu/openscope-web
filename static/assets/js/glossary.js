requirejs.config({
  baseUrl: '/assets/js',
  paths: {
    Vue: 'vue.min',
  }
});

const config = {
  baseUrl: 'http://192.168.0.16:8080'
}

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this;
    var args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      func.apply(context, args);
    }
  };
}

var SearchBox = Vue.component('SearchBox', {
  template: '<input type="text" @input="input"/>',
  methods: {
    input: debounce(function (event) {
        let search = event.target.value;
        if (!search) {
          return;
        }
        var self = this;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', `${config.baseUrl}/api/originals?filter=${search}`);
        xhr.onreadystatechange = function () {
          if (this.readyState === 4 && this.status === 200) {
            self.$emit('searched', JSON.parse(this.responseText));
          }
        };
        xhr.send();
      }, 300)
  }
});

const handledContexts = ['general', 'libreoffice', 'mozilla'];
const partsOfSpeech = {
  'verb': 'ige',
  'noun': 'fn.',
  'adjective': 'mn.',
  'determiner': 'det.',
  'adverb': 'hsz.',
  'pronoun': 'nm.',
  'conjunction': 'ksz.',
  'interjection': 'isz.'
};

const vueApp = new Vue({
  el: '#vapp',
  props: {
    items: null
  },
  methods: {
    searched: function (items) {
      this.items = items;
      for (let i = 0; i < this.items.length; i++) {
        let item = this.items[i];
        let converter = new showdown.Converter({
          strikethrough: true
        });

        if (partsOfSpeech[item.partOfSpeech]) {
          item.partOfSpeech = partsOfSpeech[item.partOfSpeech];
        }

        let context = item.translation.context;
        if (context) {
          if (handledContexts.includes(context)) {
            item.translation.contextClass = item.translation.context;
          } else {
            item.translation.contextClass = 'secondary';
          }
        }

        if (item.translation.comment) {
          item.translation.comment = converter.makeHtml(item.translation.comment);
        }
      }
    }
  }
});